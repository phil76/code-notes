import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Note from 'App/Models/Note';

export default class NotesController
{
    /**
     * Show all notes
     */
    public async index({ request, view, auth }: HttpContextContract)
    {
        const page = request.input('page', 1)

        const notes = await Note.query().orderBy('createdAt', "desc").paginate(page, 10)

        const isLogged = await auth.check()

        if (isLogged)
        {
            const user = await auth.authenticate()

            const isAdmin = await user?.role(user) === 'admin' ? true : false;

            return view.render('notes/index', { notes, isAdmin, isLogged })
        }

        const isAdmin = false

        return view.render('notes/index', { notes, isAdmin, isLogged })
    }

    /**
     * Filters the notes
     * @param form
     */
    public async search({ request, response, view, auth }: HttpContextContract)
    {
        const postSchema = schema.create({
            q: schema.string({ escape: true, trim: true }),
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        if (!data.q.length)
        {
            response.redirect().back()
        }

        const page = request.input('page', 1)

        const notes = await Note.query()
            .where('title', 'like', `%${data.q}%`)
            .orWhere('content', 'like', `%${data.q}%`)
            .orWhere('author', 'like', `%${data.q}%`)
            .paginate(page, 10)

        const isLogged = await auth.check()

        if (isLogged)
        {
            const user = await auth.authenticate()

            const isAdmin = await user?.role(user) === 'admin' ? true : false;

            return view.render('notes/index', {
                notes,
                searchField: data.q,
                isAdmin,
                isLogged
            })
        }

        return view.render('notes/index', {
            notes,
            searchField: data.q,
            isAdmin: false,
            isLogged
        })
    }

    /**
     * Show the page to edit a note
     * @param id
     */
    public async edit({ params, view }: HttpContextContract)
    {
        if (isNaN(params.id))
        {
            return view.render('errors/forbidden')
        }

        const note = await Note.findOrFail(params.id)

        return view.render('notes/update', { note, isLogged: true })
    }

    /**
     * Add a note
     * @param form 
     */
    public async store({ request, response, session, auth }: HttpContextContract)
    {
        const postSchema = schema.create({
            inputTitle: schema.string({ escape: true, trim: true }),
            inputContent: schema.string(),
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        if (data.inputTitle.length && data.inputContent.length)
        {
            const note = await Note.create({
                title: data.inputTitle,
                content: data.inputContent,
                author: auth.user?.username,
                userId: auth.user?.id
            })

            if (note)
            {
                session.flash('success', 'Note created successfully')

                response.redirect().back()
            }
        }
    }

    /**
     * Delete a note
     * @param id 
     */
    public async delete({ params, response, session, auth, view }: HttpContextContract)
    {
        if (!isNaN(params.id))
        {
            const note = await Note.findOrFail(params.id)

            const admin = await auth.user?.role(auth.user) === 'admin' ? true : false;

            if (admin || note.userId === auth.user?.id)
            {
                await note.delete()

                response.redirect().back()

                session.flash('success', 'Note deleted successfully')
            }
        }

        return view.render('errors/forbidden')
    }

    /**
     * Update a note
     * @param form
     */
    public async update({ view, request, response, session, auth }: HttpContextContract)
    {
        const postSchema = schema.create({
            inputTitle: schema.string({ escape: true, trim: true }),
            inputContent: schema.string(),
            id: schema.number()
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        const note = await Note.findOrFail(data.id)

        if (!note)
        {
            return view.render('errors/forbidden')
        }

        const user = await auth.authenticate()

        const isAdmin = await user.role(user) === 'admin' ? true : false;

        if (note.userId !== user.id && !isAdmin)
        {
            return view.render('errors/forbidden')
        }

        if (data.inputContent.length && data.inputTitle.length)
        {
            note.title = data.inputTitle
            note.content = data.inputContent

            await note.save()

            response.redirect().back()

            session.flash('success', 'Note updated successfully')
        }
    }
}
