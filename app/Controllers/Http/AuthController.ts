import User from 'App/Models/User'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Note from 'App/Models/Note'
import Role from 'App/Models/Role'

export default class AuthController
{
    /**
     * Register a new user
     * @param form 
     */
    public async register({ request, auth, response }: HttpContextContract)
    {
        // Validate user details
        const validationSchema = schema.create({
            username: schema.string({ trim: true, escape: true }, [
                rules.unique({ table: 'users', column: 'username' }),
            ]),
            password: schema.string({ trim: true }, [
                rules.confirmed(),
            ]),
        })

        const userDetails = await request.validate({
            schema: validationSchema,
        })

        // Create the new user
        const user = new User()
        user.username = userDetails.username
        user.password = userDetails.password

        await user.save()

        // Create a role for the user
        const role = new Role()
        role.name = 'regular'
        role.userId = user.id

        await role.related('user').associate(user)
        await role.save()

        // Login the user
        await auth.login(user)

        // Redirect to home
        response.redirect('/')
    }

    /**
     * Login the user
     * @param form 
     */
    public async login({ auth, request, response }: HttpContextContract)
    {
        const username = request.input('username')

        const password = request.input('password')

        await auth.attempt(username, password)

        if (auth)
        {
            response.redirect('/')
        }
    }

    /**
     *  Logout the user
     */
    public async logout({ auth, response, session }: HttpContextContract)
    {
        await auth.logout();

        session.flash({ notification: 'Logged out successfully' });

        return response.redirect('/');
    }

    /**
     * Show the user dashboard
     */
    public async dashboard({ auth, view }: HttpContextContract)
    {
        const user = await auth.authenticate()

        const notes = await Note.query().where('author', 'like', user.username)

        const count = await Note.query().count('*')

        const total = count[0]['count(*)']

        return view.render('dashboard', { user, notes, total, isLogged: true })
    }
}
