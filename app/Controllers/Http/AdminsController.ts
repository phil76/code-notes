import Database from "@ioc:Adonis/Lucid/Database";
import Role from "App/Models/Role";
import Stat from "App/Models/Stat";
import User from "App/Models/User";

export default class AdminsController
{
    public async index({ auth, view })
    {
        const isLogged = await auth.check()

        const user = await auth.authenticate()

        const isAdmin = await user?.role(user) === 'admin' ? true : false;

        if (!isLogged)
        {
            return view.render('errors/not-found', {
                isAdmin,
                isLogged
            })
        }

        if (isAdmin)
        {
            const users = await Database.rawQuery('select users.username, users.id as userId, roles.id as `roleId`, roles.name as `role` from `users` inner join `roles` on roles.user_id = users.id;')

            const stats = await Stat.findOrFail(1)

            const notesCount = await Database.rawQuery('SELECT COUNT(*) as `count` FROM notes')
            console.log(notesCount)

            return view.render('admin_panel', {
                isAdmin,
                isLogged,
                users,
                stats,
                notesCount: notesCount[0].count
            })
        }

        return view.render('errors/not-found', {
            isAdmin,
            isLogged
        })
    }

    public async changeRole({ params, response, view })
    {
        if (isNaN(params.id))
        {
            return view.render('errors/forbidden')
        }

        const role = await Role.findOrFail(params.id)

        if (role.name === 'regular')
        {
            role.name = 'admin'
        }
        else
        {
            role.name = 'regular'
        }

        await role.save()

        response.redirect().back()
    }

    public async deleteUser({ params, response, view })
    {
        if (isNaN(params.id))
        {
            return view.render('errors/forbidden')
        }

        const user = await User.findOrFail(params.id)

        await user.delete()

        response.redirect().back()
    }

    public async copycode()
    {
        const stat = await Stat.findOrFail(1)

        stat.codeCopied += 1

        stat.save()
    }
}
