import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import
{
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
} from '@ioc:Adonis/Lucid/Orm'
import Note from './Note'
import Role from './Role'

export default class User extends BaseModel
{
  // columns
  @column({ isPrimary: true })
  public id: number

  @column()
  public username: string

  @column({ serializeAs: null })
  public password: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Note)
  public notes: HasMany<typeof Note>

  // methods
  @beforeSave()
  public static async hashPassword(user: User)
  {
    if (user.$dirty.password)
    {
      user.password = await Hash.make(user.password)
    }
  }

  /**
   * name
   */
  public async role(user: User)
  {
    const role = await Role.findBy('userId', user.id)

    if (role)
    {
      return role.name
    }

    return 'visitor';
  }
}
