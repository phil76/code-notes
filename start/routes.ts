/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

// index
// Route.get('/', async ({ view, auth }) =>
// {
//     const state = {
//         isLogged: await auth.check()
//     }

//     return view.render('index', state)
// })

// about
Route.get('/about', async ({ view, auth }) =>
{
    const state = {
        isLogged: await auth.check()
    }

    return view.render('about', state)
})

// notes
Route.get('/', 'NotesController.index').middleware('silentAuth')

Route.post('/', 'NotesController.search').middleware('silentAuth')

Route.get('/add', async ({ view, auth }) =>
{
    const state = {
        isLogged: await auth.check()
    }

    return view.render('notes/add', state)
}).middleware('auth')

Route.post('/add', 'NotesController.store').middleware('auth')

Route.get('/edit/:id', 'NotesController.edit').middleware('auth')

Route.post('/update', 'NotesController.update').middleware('auth')

Route.get('/delete/:id', 'NotesController.delete').middleware('auth')

// register
Route.on('register').render('register')
Route.post('register', 'AuthController.register')

// login
Route.on('login').render('login')
Route.post('/login', 'AuthController.login')

// logout
Route.get('logout', 'AuthController.logout').middleware('auth')

// user dashboard
Route.get('/dashboard', 'AuthController.dashboard').middleware('auth')

// admin panel
Route.get('/admin-panel', 'AdminsController.index').middleware('auth')
Route.get('/admin-panel/role/:id', 'AdminsController.changeRole').middleware('auth')
Route.get('/admin-panel/user/:id', 'AdminsController.deleteUser').middleware('auth')

// copy note counter
Route.get('server', 'AdminsController.copycode')