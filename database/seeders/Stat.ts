import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Stat from 'App/Models/Stat'

export default class StatSeeder extends BaseSeeder {
  public async run () {
    await Stat.createMany([
      {
        codeCopied: 0,
      }
    ])
  }
}
