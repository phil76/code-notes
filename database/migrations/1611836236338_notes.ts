import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Notes extends BaseSchema
{
    protected tableName = 'notes'

    public async up()
    {
        this.schema.createTable(this.tableName, (table) =>
        {
            table.increments('id').primary()
            table.string('title').notNullable()
            table.text('content').notNullable()
            table.string('author').notNullable()
            table.integer('user_id').references('id').inTable('users')
            table.timestamps(true)
        })
    }

    public async down()
    {
        this.schema.dropTable(this.tableName)
    }
}
